package com.company.demo.web.utils;

import com.haulmont.cuba.core.global.Resources;
import com.haulmont.cuba.gui.components.SourceCodeEditor;
import com.haulmont.cuba.web.toolkit.ui.CubaSourceCodeEditor;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class PresentationUtils {
    @Inject
    private Resources resources;

    public void setupCodeEditor(SourceCodeEditor codeEditor) {
        codeEditor.setEditable(false);
        codeEditor.setMode(SourceCodeEditor.Mode.XML);
        codeEditor.setShowPrintMargin(false);
    }

    public String getValueFromFile(String src, int beginIndex, int fromLine, int toLine) {
        String fileContent = resources.getResourceAsString(src);
        if (StringUtils.isEmpty(fileContent)) {
            throw new RuntimeException("Source file not found");
        }

        String[] lines = fileContent.split("\n");
        Collection<String> trimmed = Arrays.stream(ArrayUtils.subarray(lines, fromLine - 1, toLine))
                .map(o -> {
                    String str = ((String) o);
                    return StringUtils.isNotBlank(str) ? str.substring(beginIndex) : "";
                }).collect(Collectors.toList());
        return StringUtils.join(trimmed, "\n");
    }
}
