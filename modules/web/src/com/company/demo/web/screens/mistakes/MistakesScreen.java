package com.company.demo.web.screens.mistakes;

import com.company.demo.web.utils.PresentationUtils;
import com.haulmont.cuba.gui.components.AbstractWindow;
import com.haulmont.cuba.gui.components.SourceCodeEditor;

import javax.inject.Inject;
import java.util.Map;

public class MistakesScreen extends AbstractWindow {
    private static final int BEGIN_INDEX = 6 * 4; // 6 tabs, one tab = 4 spaces
    private static final String SRC = "com/company/demo/web/screens/mistakes/mistakes-screen.xml";

    @Override
    public void init(Map<String, Object> params) {
        initRelativeSizeTab();
        initScrollBoxTab();
    }

    @Inject
    private PresentationUtils presentationUtils;

    private void setupCodeEditor(SourceCodeEditor codeEditor, int fromLine, int toLine) {
        presentationUtils.setupCodeEditor(codeEditor);
        codeEditor.setValue(presentationUtils.getValueFromFile(SRC, BEGIN_INDEX, fromLine, toLine));
    }

    @Inject
    private SourceCodeEditor relativeSizeCode1;
    @Inject
    private SourceCodeEditor relativeSizeCode2;

    private void initRelativeSizeTab() {
        setupCodeEditor(relativeSizeCode1, 20, 23);
        setupCodeEditor(relativeSizeCode2, 39, 43);
    }

    @Inject
    private SourceCodeEditor aligningCode;

    private void initScrollBoxTab() {
        setupCodeEditor(aligningCode, 65, 68);
    }
}