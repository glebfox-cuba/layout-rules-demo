package com.company.demo.web.screens.examples;

import com.company.demo.web.utils.PresentationUtils;
import com.haulmont.cuba.gui.components.AbstractWindow;
import com.haulmont.cuba.gui.components.SourceCodeEditor;

import javax.inject.Inject;
import java.util.Map;

public class ExamplesScreen extends AbstractWindow {
    private static final int BEGIN_INDEX = 6 * 4; // 6 tabs, one tab = 4 spaces
    private static final String SRC = "com/company/demo/web/screens/examples/examples-screen.xml";

    @Override
    public void init(Map<String, Object> params) {
        initEqualSpaceTab();
        initContentBaseSizeTab();
        initRelativeSizeTab();
        initExpandTab();
        initFormTab();
        initMarginTab();
        initSpacingTab();
        initAligningTab();
    }

    @Inject
    private PresentationUtils presentationUtils;

    private void setupCodeEditor(SourceCodeEditor codeEditor, int fromLine, int toLine) {
        presentationUtils.setupCodeEditor(codeEditor);
        codeEditor.setValue(presentationUtils.getValueFromFile(SRC, BEGIN_INDEX, fromLine, toLine));
    }

    @Inject
    private SourceCodeEditor equalSpaceCode;

    private void initEqualSpaceTab() {
        setupCodeEditor(equalSpaceCode, 20, 25);
    }

    @Inject
    private SourceCodeEditor contentBaseSizeCode;

    private void initContentBaseSizeTab() {
        setupCodeEditor(contentBaseSizeCode, 47, 50);
    }

    @Inject
    private SourceCodeEditor relativeSizeCode;

    private void initRelativeSizeTab() {
        setupCodeEditor(relativeSizeCode, 72, 75);
    }

    @Inject
    private SourceCodeEditor expandCode;

    private void initExpandTab() {
        setupCodeEditor(expandCode, 97, 104);
    }

    @Inject
    private SourceCodeEditor formCode;

    private void initFormTab() {
        setupCodeEditor(formCode, 126, 137);
    }

    @Inject
    private SourceCodeEditor marginCode1;
    @Inject
    private SourceCodeEditor marginCode2;
    @Inject
    private SourceCodeEditor marginCode3;

    private void initMarginTab() {
        setupCodeEditor(marginCode1, 159, 162);
        setupCodeEditor(marginCode2, 178, 182);
        setupCodeEditor(marginCode3, 198, 202);
    }

    @Inject
    private SourceCodeEditor spacingCode1;
    @Inject
    private SourceCodeEditor spacingCode2;

    private void initSpacingTab() {
        setupCodeEditor(spacingCode1, 224, 229);
        setupCodeEditor(spacingCode2, 245, 251);
    }

    @Inject
    private SourceCodeEditor aligningCode1;
    @Inject
    private SourceCodeEditor aligningCode2;

    private void initAligningTab() {
        setupCodeEditor(aligningCode1, 273, 277);
        setupCodeEditor(aligningCode2, 293, 297);
    }
}